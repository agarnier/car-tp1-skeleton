package fr.lille.univ.car.tp1;

import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    
    private static int PORT = 1024;
    
    public static void main(String... args) {
        ServerSocket serverSocket = new ServerSocket(PORT);
        
        while (true) {
            Socket clientSocket = serverSocket.accept();
            new Thread(new ClientSession(clientSocket)).start();
        }
    }
}
