package fr.lille.univ.car.tp1;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;

import java.net.Socket;

public class ClientSession implements Runnable {

    private BufferedReader in;
    private PrintStream out; // out.println(...)

    public ClientSession(Socket socket) {
        in = new BufferedReader(new InputStreamReader(
                socket.getInputStream()));
        out = new PrintStream(socket.getOutputStream());
        out.println(220);
    }

    @Override
    public void run() {
        while (true) {
            process(in.readLine());
        }
    }

    private static void process(String request) {
        System.out.println(request);
    }
}
